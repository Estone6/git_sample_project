1 )Create a directory called git_sample_project and cd to git_sample_project

   $ mkdir git_sample_project
   $ cd git_sample_project


2) Initialize a git repo.
  $ git init

3) Create the following files a.txt, b.txt, c.txt
   $ touch a.txt
   $ touch b.txt
   $ touch c.txt


4 )Add some arbitrary content to the above files.
  $ vi a.txt
  $ vi b.txt
  $ vi c.txt

5 )Add a.txt and b.txt to the staging area
   $ git add a.txt b.txt

6) Run git status. Understand what it says.
  $ git status

7) Commit a.txt and b.txt with the message "Add a.txt and b.txt"
   $ git commit -m "Add a.txt and b.txt"
   

8)Run git status. Understand what it says
  $ git status

9) Run git log
  $ git log


10)Add and Commit c.txt
   $ git add c.txt
   $ git commit -m "Add c.txt"
   

11)Create a project on GitLab.
    git remote add origin https://gitlab.com/Estone6/git_sample_project.git

12)Push your code to GitLab
    $ git push -u origin master


13) Clone your project in another directory called git_sample_project_2
    cd ..
    mkdir git_sample_project_2
    $ git clone https://gitlab.com/Estone6/git_sample_project.git git_sample_project_2



14) In git_sample_project_2, add a file called d.txt
    $ cd git_sample_project_2
    touch d.txt


15) Commit and push d.txt
   $ git add d.txt
   $ git commit -m "Add d.txt"


16) cd back to git_sample_project
    $ cd ..
    $ cd git_sample_project


17)Pull the changes from GitLab
  $ git pull

18)Copy git_drill_part_1.txt to git_sample_project. Commit and Push it on GitLab
    

19)Copy cli_drill_part_1.txt to git_sample_project. Commit and Push it on GitLab
